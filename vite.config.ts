import { defineConfig, type UserConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

export default defineConfig(({ mode }) => {
  const __DEV__ = mode === 'development'

  return {
    base: __DEV__ ? '/' : '/sfc-playground/',
    plugins: [vue()],
    define: {
      __COMMIT__: JSON.stringify("xxx"),
      __VUE_PROD_DEVTOOLS__: JSON.stringify(true)
    },
    optimizeDeps: {
      exclude: ['@vue/repl']
    }
  }
})
