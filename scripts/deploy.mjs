import shell from 'shelljs'

const run = (command) => shell.exec(command)

run('npm run build')
shell.cd('dist')
run('echo > .nojekll')
run('git init')
run('git checkout -B main')
run('git add -A')
run(`git commit -m 'deploy'`)
run(`git push -f git@gitee.com:vuemi/sfc-playground.git main:gh-pages`)
